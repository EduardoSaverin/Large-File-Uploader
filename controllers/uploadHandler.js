var formidable = require("formidable");
var fs = require('fs');
var path = require('path');

module.exports = function(request, response) {
  var form = new formidable.IncomingForm();

  //Allow multiple files
  form.multiples = true;

  //Set Upload Directory
  form.uploadDir = path.join(__dirname, '../uploads');

  //every time a file has been uploaded successfully,rename it to it's original name
  form.on('file', function(field, file) {
    console.log("Files");
    fs.rename(file.path, path.join(form.uploadDir, file.name));
  });

  form.on('end', function() {
    response.end('Success');
  });

  form.parse(request);
}
