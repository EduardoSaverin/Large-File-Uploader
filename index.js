//Variable declaration.
var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs-extra');
var path = require('path');
var uploadHandler = require('./controllers/uploadHandler');

//Setting Template Rendering Engine.
//This is Embedded JS.
app.set("view engine", "ejs");

router.get("/", function(request, response) {
  response.render("home");
});

router.post("/", function(request, response) {
  uploadHandler(request, response);
});

//Setting static path to look up files.
app.use(express.static('./public'));
app.use(router);
app.listen(process.env.PORT || 3000);
console.log("Listening on port 3000");
